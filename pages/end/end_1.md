---
layout: image-right

# the image source
image: https://source.unsplash.com/collection/94734566/1920x1080

# a custom class name to the content
class: my-cool-content-on-the-left 
---

# Thanks For Appreciation

用代码表达言语的魅力，用代码书写山河的壮丽|

Your flustered, complaining, bitter face, daring to take on things, shirking, making excuses, unclear logic, lack of feedback, informality, non-brain, and non-intentional actions will betray you. Sunshine, calm, Optimism, resistance to blows, clear thinking, courage to contribute, tolerance to loneliness, not afraid of making mistakes, progress, and every day of hard work will lead to a future that surprises even yourself.
