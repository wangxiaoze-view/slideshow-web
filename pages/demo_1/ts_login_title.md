---
transition: fade-out
---

# 巧用Ts枚举(登录动态标题)


> 思考：登录功能大致分为：手机号，二维码，注册，重置密码等；再点击不同模式的情况下，如何优雅的显示不同的标题？
>

<br>


<img src="https://qiniu.wangxiaoze.wang/web-ppt/web_ppt_1.png" alt="登录" style="width: 70%;margin: auto" >

---
layout: left-right
transition: fade-out
---

## 常规写法

<br/>
<template #left>
```vue
<template>
    {{title}}
    <button @click="setStatus(0)">登录</button>
    <button @click="setStatus(1)">忘记密码</button>
    <button @click="setStatus(2)">手机登录</button>
    <button @click="setStatus(3)">二维码登录</button>
    <button @click="setStatus(4)">注册</button>
</template>
```
</template>
<template #right>
```ts {1|2-9|10-12|13-16|all}
import {ref} from 'vue'
// 定义配置
const config = {
    0: '登录',
    1: '忘记密码',
    2: '手机登录',
    3: '二维码登录',
    4: '注册',
}
// 定义初始化status, 0代表登录按钮
const status = ref(0)
const title = ref('')
const setStatus = (status: numer): void => {
    status.value = status;
    title.value = config[status]
}
```
</template>

---
layout: page-full
transition: fade-out
---

## ts枚举类型

<br/>

```vue
<template>
    {{title}}
    <button @click="setStatus(LoginStatus.LOGIN)">登录</button>
    <button @click="setStatus(LoginStatus.RESET_PASSWORD)">忘记密码</button>
    <button @click="setStatus(LoginStatus.MOBILE)">手机登录</button>
    <button @click="setStatus(LoginStatus.QR_LOGIN)">二维码登录</button>
    <button @click="setStatus(LoginStatus.REGISTER)">注册</button>
</template>
```

```ts {1-7|8-9|10-19|all}
enum LoginStatus {
    LOGIN, // 登录
    RESET_PASSWORD, // 忘记密码
    MOBILE, // 手机登录
    QR_LOGIN, // 二维码登录
    REGISTER, // 注册
}
// 自己编写hooks
const {getStatus} = useStatus(0)
const title = computed(() => {
    const titleObj = {
        [LoginStatus.LOGIN]: '登录',
        [LoginStatus.RESET_PASSWORD]: '忘记密码',
        [LoginStatus.MOBILE]: '手机登录',
        [LoginStatus.QR_LOGIN]: '二维码登录',
        [LoginStatus.REGISTER]: '注册',
    }
    return titleObj(unref(getStatus))
})
```
---
layout: page-full
transition: fade-out
---
## 表单校验

<br/>

```ts
// 一个hooks methods 解决多种登录方案的表单校验
const useGetRules = (status: number) => {
    // 登录表单校验
    const loginRule = unRef(getLoginRule)
    // 重置密码表单校验
    const resetRule = unRef(getResetRule)
    // 根据状态区分
    switch (status) {
        case LoginStatus.LOGIN:
            return loginRule;
        case LoginStatus.RESET_PASSWORD:
            return resetRule;
        case LoginStatus.MOBILE:
            return {}
        default: 
            return { user: '', passWord: '' }
    }
}
```