---
transition: fade-out
---

# tab切换优雅请求


> 思考：**tab**功能在切换时会出发请求？针对于该做法如何优化？

<br/>

<img src="https://qiniu.wangxiaoze.wang/hexo-blog/tabs_loading.png" alt="tab" style="width: 80%;margin: auto">


---
transition: fade-out
---

## 常规写法

我们要清楚，常规做法与优化之后的做法的好处在哪里？

常规做法无非是这样：

```ts
const index = ref(0) // 设置tab 索引
// 设置config
const tabs = [ 
    {name: '男装', id: 1},
    {name: '女装', id: 2},
    {name: '电器', id: 3},
    {name: '运动', id: 4},
]
const proxy = (id: number) => {
    // ... 这里是请求
}
// 切换tab
const changeTab = (index: number) => {
    proxy(tabs[index].id); // 每次都会获取id,
}
```
<div v-click class="text-xl p-2">
<span style="color: orangered;">结论：扩展性不强，如果项目中多类似功能的话，那不是每个页面都会使用一次？不符合组件封装复用规范；后期维护性难；</span>
</div>


---
transition: fade-out
layout: page-full
---

## 优化方案: 封装统一 **hooks**, 发挥 **hooks**在 **vue**中的重要性
<br/>

```ts
import {computed, watchEffect} from 'vue'
// 初始ID
const id = ref(1)
// 改变ID
const changeId = () => id.value++; 
// 监听url
const url = computed(() => 'https://api-xxxxx.com' + id.value);
// hooks
const { data, error, doFetch } = useFetch(url)
```

```ts
export function useFetch(url) {
    // 数据
    const data = ref(null)
    // 失败
    const error = ref(null)
    // 请求对应操作
    async function doFetch() {
        data.value = null;
        error.value = null;
        // 做兼容，可能是ref的值，也可能是普通值
        let urlVal = unRef(url);
        try {
            const res = await fetch(urlVal)
            data.value = res.json();
        } catch (err) {
            error.value = err;
        }
        // 请求api接口... 如需针对所有tab, 这里可以做一层封装📦
    }
    // isRef: 是否是ref的值；
    isRef(url) ? watchEffect(doFetch) : doFetch();
    return { data, error, doFetch }
}
```
<div v-click class="text-xl p-2">
<span style="color: green;">结论：扩展性较强，对类似选项卡都可以使用该 hooks；</span>
效果图如下：
</div>

---
layout: iframe
# the web page source
url: https://web-demos.wangxiaoze.wang/#/vue/tabsLoading
---