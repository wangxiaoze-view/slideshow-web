---
transition: fade-out
---

# Personal Project

目前个人项目分为 **web-blog，web-demos，simple-admin，web-utils-js, temp-pro, web-ppt**

<br>

- 📝 [**web-blog**](https://www.wangxiaoze.wang/) - 记录工作生活中的一些疑难点；
- 🎨 [**web-demos**](https://web-demos.wangxiaoze.wang/#/) - 总结一些前端的功能点
- 🤹 [**simple-admin**](https://simple-admin.wangxiaoze.wang/) - 一个简单配置主题的中后台模板
- 🛠 [**web-utils-js**](https://www.npmjs.com/package/web-utils-js) - 封装底层源码(工具库)，常用的正则校验，数学的计算等等；适用于 *vue, node, 浏览器*
- 📤 [**temp-pro**](https://www.npmjs.com/package/temp-pro) - 一个*node*下载项目模板的工具，包含*Admin, H5, uni-app, flutter(暂未整理)， node+mongoose, nest+mysql*;
- ‍💻 [**web-ppt**](https://web-ppt.wangxiaoze.wang) - 用于日常整理，分享前端优化写法合集
