---
theme: seriph
background: https://source.unsplash.com/collection/94734566/1920x1080
class: text-center
highlighter: shiki
lineNumbers: true
info: false
drawings:
  persist: false
transition: slide-left
titleTemplate: '%s - 优秀技能点'
title: 王小泽PPT
favicon: 'https://qiniu.wangxiaoze.wang/hexo-blog/wechat_au.jpeg'
---

# Welcome to Wang Xiaoze PPT

Share some front-end excellent writing collections

<div class="pt-12">
  <span @click="$slidev.nav.next" class="px-2 py-1 rounded cursor-pointer" hover="bg-white bg-opacity-10">
    Start <carbon:arrow-right class="inline"/>
  </span>
</div>

<div class="abs-br m-6 flex gap-2">
  <a href="https://github.com/wangxiaoze-view/slideshow-web.git" target="_blank" alt="GitHub"
    class="text-xl slidev-icon-btn opacity-50 !border-none !hover:text-white">
    <carbon-logo-github />
  </a>
</div>

<!-- 为什么做ppt -->
---
src: ./pages/welcome/why.md
---

<!-- 目前个人项目 -->
---
src: ./pages/welcome/product.md
---

<!-- 目录 -->
---
src: ./pages/welcome/toc.md
---

<!-- 案例一：ts登录标题 -->
---
src: ./pages/demo_1/ts_login_title.md
---

<!-- 案例二：tab切换请求 -->
---
src: ./pages/demo_1/tab_proxy.md
---


<!-- 结束 -->
---
src: ./pages/end/end_1.md
---